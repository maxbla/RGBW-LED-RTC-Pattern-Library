#include <Adafruit_NeoPixel.h> //library for LED strip
#include <Wire.h>              //Library for RTC interfacing
#include "RTClib.h"            //Library for getting/setting date/time
#include <math.h>

/**Pin number for Vcc pin of neopixel data line*/
#define PIN 9

/**Number of LEDs on attached neopixel strip*/
#define NUM_LED 240

/**Pin for mic input*/
#define MIC_PIN A0

/**Array with days of the week indexed starting with - on Monday. Used to
 * translate day number into day string for nice printing. This could
 * alternativly be an enum, but an array gets the job done.
 */
const char DaysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday","Wednesday",
                                   "Thursday", "Friday", "Saturday"};

/**Object with methods for interacting with physical neopixels. Class file
 * comes from Adafruit_NeoPixel library availible on github at
 * https://github.com/adafruit/Adafruit_NeoPixel. In the constructor,
 * Parameter 1 is the number of pixels in strip
 * Parameter 2 is the Arduino pin number (most are valid)
 * Parameter 3 contains pixel type flags, add together as needed (if you are
 * using this code with your own LEDs, you'll probably need to change this)
 */
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LED, PIN, NEO_GRBW+NEO_KHZ800);

/**Real time clock object. Initialized in setup() finction*/
RTC_DS1307 rtc;
DateTime now;
bool on;

void setup()
{
  Serial.begin(9600);
  while(!Serial)
    ;

  strip.begin();

  //powers the RTC, when it is be plugged directly into the arduino
  pinMode(A3, OUTPUT);
  pinMode(A2, OUTPUT);
  digitalWrite(A3,HIGH);
  digitalWrite(A2,LOW);
  
  pinMode(MIC_PIN, INPUT);

  if (!rtc.begin())
    Serial.println("Couldn't find RTC");
  if (!rtc.isrunning())
    Serial.println("RTC is NOT running!");

  //sets date and time on RTC to compile time
  //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));

  //sets date and time to November 25, 2017 at 1:00 am
  //rtc.adjust(DateTime(2017, 11, 25, 1, 0, 0));
  colorStrip(strip, white(0));
  on = false;
  delay(1000);
}

bool wrap_between(int start, int finish, int test) 
{
  if (start<=finish) //not wrapping
  {
    if (start<=test && start<finish) {;}
  }
    return true;
  if (start>finish) {;}
}

/**
 * Tests if now occurs between start_hour and end_hour. Uses a 24-hour clock.
 * Values wrap, so for example 2 is between 23 and 3.
 * @param now the DateTime object to test
 * @param start_hour the start of the hour range (inclusive)
 * @param end_hour the end of the hour range (exclusive)
 * @return truth value of the statement now is between start_hour and end_hour
 */
bool betweenTimes(DateTime now, int start_hour, int end_hour)
{
  if (start_hour <= end_hour)
  {
    if (now.hour() >= start_hour && now.hour() < end_hour)
      return true;
  }
  else
  {
    if (now.hour() >= start_hour || now.hour() < end_hour)
      return true;
  }
  return false;
}

/**
 * Tests if now occurs between the start_minute-th minute of start_hour
 * and the end_minute-th minute of end_hour. Uses a 24-hour clock.
 * Values wrap, so for example 2 is between 23 and 3.
 * @param now the DateTime object to test
 * @param start_hour the start of the hour range (inclusive)
 * @param end_hour the end of the hour range (exclusive)
 * @return truth value of the statement now is between start_hour and end_hour
 */
bool betweenTimes(DateTime now, int start_hour, int end_hour,
                   int start_minute, int end_minute)
{
  const int MINUTES_PER_HOUR = 60;
  //minute of day (out of 1440) from passed now object
  int now_minute = now.hour()*MINUTES_PER_HOUR + now.minute();
  int begin_minute = start_hour*MINUTES_PER_HOUR + start_minute;
  int finish_minute = end_hour*MINUTES_PER_HOUR + end_minute;
  if (begin_minute <= now_minute && now_minute < finish_minute)
  {
    return true;
  }
  else if (begin_minute > finish_minute && now_minute > finish_minute) {

  }
  return false;
}

/**Limits the range of arg to between l_bound and u_bound. Specifically if arg
 * is less than l_bound, assigns value of l_bound to arg and if arg is greater
 * than u_bound, assigns value of u_bound to arg.
 * @param arg the int that will have its range limited
 */
void limitRange(int& arg, int l_bound, int u_bound)
{
  if (arg<l_bound)
    arg = l_bound;
  if (arg>u_bound)
    arg = u_bound;
}

/**
 * Colors the whole strip one color.
 * @param strp the strip to color
 * @param c the color to make the strip
 */
void colorStrip(Adafruit_NeoPixel& strp, uint32_t c)
{
  for(int i=0; i<strp.numPixels(); i+=1)
    strp.setPixelColor(i,c);
}

/**
 * Colors one LED, then skips several LEDs, then colors one, etc.
 * @param strp the strip to color
 * @param c the color to make the strip
 * @param every_n colors the first pixel, then skips n-1, colors the next pixel
 */
void colorStrip(Adafruit_NeoPixel& strp, uint32_t c, int every_n)
{
  for(int i=0; i<strp.numPixels(); i+=every_n)
    strp.setPixelColor(i,c);
}

/**
 * Colors one LED, then skips several LEDs, then colors one, et cetera. Colors
 * the first pixel, then greedily makes ratio on LEDs to off LEDs close to frac
 * number of on pixels as close to possible to
 * @param strp the strip to color
 * @param c the color to make the strip
 * @param frac the fraction of LEDs to keep on
 */
void colorStrip(Adafruit_NeoPixel& strp, uint32_t c, float frac)
{
  int on, tot;
  for(tot=0; tot<strp.numPixels(); tot+=1) {
    if ((float)on/(float)tot < frac) {
      (strp).setPixelColor(tot, c);
      on += 1;
    }
    else
      strp.setPixelColor(tot, strp.Color(0,0,0,0));
  }
}

/**
 * Colors a contiguous segment of LEDs, leaving the others off. Wraps around
 * the end of the strip - for example if start is 79, end is 3 and the strip
 * is 80 LEDs long, this will light up LEDs with index 79,80,1 and 2.
 * @param strp the strip to color
 * @param c the color to make the strip
 * @param start the index of the LED to start at (inclusive)
 * @param end the index of the LED to end ar (exclusive)
 */
void colorStrip(Adafruit_NeoPixel& strp, uint32_t c, int start, int end)
{
  for(int i=start; i<end; i+=1)
      strp.setPixelColor(i, c);
  if (start > end)
  {
    for (int i=start; i<end || i>=start; i+=1)
    {
      strp.setPixelColor(i, c);
    }
  }
}

/**
 * Colors half the strip one color and half the strip another color
 * @param strp the strip to color
 * @param c1 the color for the first half of the strip
 * @param c2 the color for the second half of the strip
 */
void halfAndHalf(Adafruit_NeoPixel& strp, uint32_t c1, uint32_t c2)
{
  colorStrip(strp, c1, 0                 , strp.numPixels()/2 );
  colorStrip(strp, c2, strp.numPixels()/2, strp.numPixels()   );
}

/**
 * Colors the strip with a distinct random color for every pixel. Only
 * randomizes the red green and blue components, not white.
 * @param strp the strip to color
 * @param wait the time in milliseconds to wait between changing every pixel's
 * color.
 * @param brightness the maximum brightness of any R/G/B component
 */
void randomStrip(Adafruit_NeoPixel& strp, int brightness) {
  int rgb[3], i, j;
  for (i=0; i<strp.numPixels(); i+=1) {
    for(j=0; j<3; j++)
      rgb[j] = random(brightness);
    strp.setPixelColor(i,strp.Color(rgb[0],rgb[1],rgb[2],0));
  }
}

/**
 * Changes what pattern is playing on the neopixel strip depending on the
 * time of day. Often uses an unmoving white pattern.
 * @param strp Adafruit_NeoPixel strip object that is altered by this function
 * @param now the DateTime object to test
 */
void timeLight(Adafruit_NeoPixel& strp, DateTime now)
{
  if(now.dayOfTheWeek()==5 && now.hour()>=17 && now.hour()<20)
  {
    randomStrip(strp,200);
    delay(100);
  }
  else if(now.hour() >= 22 || now.hour()<1)
  {
    //stary_night(5,1,4,3600*100);
    colorStrip(strp,strp.Color(10,0,0,20));
  }
  else if(now.hour()>=8 && now.hour()<17)
  {
    colorStrip(strp,strp.Color(10,0,0,100));
  }
  else if(now.hour()<8)
  {
    colorStrip(strp,strp.Color(0,0,0,0));
  }
  else if(now.hour()>17 && now.hour()<22)
  {
    colorStrip(strp,strp.Color(50,0,0,200));
  }
}

/**Returns white color of brightness brightness. Clips brightness so it is in
 * the interval [0,255]. Uses the Color method from Adafruit_NeoPixel library.
 * @param brightness how bright the color will be from 0 to 255 (inclusive)
 * @return 32-bit unsigned integer representing the color generated.
 */
uint32_t white(int brightness)
{
  limitRange(brightness,0,255);
  return Adafruit_NeoPixel::Color(0,0,0,brightness);
}

/**Returns redwhite color of brightness brightness. Clips brightness so it is in
 * the interval [0,255]. Uses the Color method from Adafruit_NeoPixel library.
 * @param brightness how bright the color will be from 0 to 255 (inclusive)
 * @return 32-bit unsigned integer representing the color generated.
 */
uint32_t softWhite(int brightness)
{
  limitRange(brightness,0,255);
  return Adafruit_NeoPixel::Color(brightness/5,0,0,brightness);
}

/**Returns red color of brightness brightness. Clips brightness so it is in
 * the interval [0,255]. Uses the Color method from Adafruit_NeoPixel library.
 * @param brightness how bright the color will be from 0 to 255 (inclusive)
 * @return 32-bit unsigned integer representing the color generated.
 */
uint32_t red(int brightness)
{
  limitRange(brightness,0,255);
  return Adafruit_NeoPixel::Color(brightness,0,0,0);
}

/**Returns green color of brightness brightness. Clips brightness so it is in
 * the interval [0,255]. Uses the Color method from Adafruit_NeoPixel library.
 * @param brightness how bright the color will be from 0 to 255 (inclusive)
 * @return 32-bit unsigned integer representing the color generated.
 */
uint32_t green(int brightness)
{
  limitRange(brightness,0,255);
  return Adafruit_NeoPixel::Color(0,brightness,0,0);
}

/**Returns green color of brightness brightness. Clips brightness so it is in
 * the interval [0,255]. Uses the Color method from Adafruit_NeoPixel library.
 * @param brightness how bright the color will be from 0 to 255 (inclusive)
 * @return 32-bit unsigned integer representing the color generated.
 */
uint32_t blue(int brightness)
{
  limitRange(brightness,0,255);
  return Adafruit_NeoPixel::Color(0,0,brightness,0);
}

/**Linearly combines colors, clipping if necessary. It is garanteed that
 * there will be no clipping if factor is in the range [0,1].
 * @param color_1 a color to mix
 * @param color_2 another color to mix
 * @param factor the factor to multiply color 1 by
 * @return the linear combination factor*color_1 + (1-factor)*color_2
 */
uint32_t mixColors(uint32_t color_1, uint32_t color_2, float factor)
{
  /**unpacks seperate rgbw components*/
  uint8_t r_1,g_1,b_1,w_1;
  uint8_t r_2,g_2,b_2,w_2;

  b_1 = (uint8_t)color_1;
  g_1 = (uint8_t)(color_1 >> 8);
  r_1 = (uint8_t)(color_1 >> 16);
  w_1 = (uint8_t)(color_1 >> 24);

  b_2 = (uint8_t)color_2;
  g_2 = (uint8_t)(color_2 >> 8);
  r_2 = (uint8_t)(color_2 >> 16);
  w_2 = (uint8_t)(color_2 >> 24);
  /*---------------------------------*/

  /*use r_1 to store the return value to save RAM (I've run out before)*/
  r_1 = factor*r_1 + (1-factor)*r_2;
  g_1 = factor*g_1 + (1-factor)*g_2;
  b_1 = factor*b_1 + (1-factor)*b_2;
  w_1 = factor*w_1 + (1-factor)*w_2;
  return Adafruit_NeoPixel::Color(r_1,g_1,b_1,w_1);
}

uint32_t expMixColors(uint32_t color_1, uint32_t color_2, float factor,
                      float exponent)
{
  /**unpacks seperate rgbw components*/
  uint8_t r_1,g_1,b_1,w_1;
  uint8_t r_2,g_2,b_2,w_2;

  b_1 = (uint8_t)color_1;
  g_1 = (uint8_t)(color_1 >> 8);
  r_1 = (uint8_t)(color_1 >> 16);
  w_1 = (uint8_t)(color_1 >> 24);

  b_2 = (uint8_t)color_2;
  g_2 = (uint8_t)(color_2 >> 8);
  r_2 = (uint8_t)(color_2 >> 16);
  w_2 = (uint8_t)(color_2 >> 24);
  /*---------------------------------*/

  /*use r_1 to store the return value to save RAM (I've run out before)*/
  float coeff = pow(factor,exponent);
  r_1 = pow(factor,exponent)*r_1 + pow((1-factor),exponent)*r_2;
  g_1 = pow(factor,exponent)*g_1 + pow((1-factor),exponent)*g_2;
  b_1 = pow(factor,exponent)*b_1 + pow((1-factor),exponent)*b_2;
  w_1 = pow(factor,exponent)*w_1 + pow((1-factor),exponent)*w_2;
  return Adafruit_NeoPixel::Color(r_1,g_1,b_1,w_1);
}

/**
 * Generates a uint8_t array, storing the RGBW elements of the passed color
 * @param color Color to decompose into RGBW elements
 * @return uint8_t array representing color elements in r, g, b, w order.
 * This array was initialized with malloc(), and must be free()ed
 */
uint8_t* unpackColor(uint32_t color)
{
  uint8_t* color_arr;
  color_arr      =(uint8_t*)malloc(sizeof(uint8_t)*4);
  *color_arr     = (uint8_t)(color >> 16);
  *(color_arr++) = (uint8_t)(color >> 8);
  *(color_arr++) = (uint8_t)color;
  *(color_arr++) = (uint8_t)(color >> 24);
  return color_arr;
}

/**
 * Linearly fades from one color to another
 * @param strp Adafruit_NeoPixel strip object that is altered by this function
 * @param color_1 the color at index_1
 * @param index_1 the index to start with color_1
 * @param color_2 the color at index_2
 * @param index_2 the index to end with color_2
 */
void fade(Adafruit_NeoPixel& strp, uint32_t color_1, int index_1,
          uint32_t color_2, int index_2)
{
  float factor;
  uint32_t mixed_color;
  for (int i=index_1; i<index_2 ; i+=1)
  {
    //Serial.println((float)(i-index_1)/(index_2-index_1));
    //Serial.println(i);
    factor = (float)(i-index_1)/(index_2-index_1);
    mixed_color = mixColors(color_1,color_2,factor);
    strp.setPixelColor(i, mixed_color);
  }
  return;
}

void fade(Adafruit_NeoPixel& strp, uint32_t color_1, int index_1,
          uint32_t color_2, int index_2, float exponent)
{
  float factor, index;
  uint32_t mixed_color;
  for (int i=index_1; i<index_2 ; i+=1)
  {
    index = (float)i;
    //Serial.println((float)(i-index_1)/(index_2-index_1));
    //Serial.println(i);
    factor = (float)(index-index_1)/(index_2-index_1);
    mixed_color = expMixColors(color_1,color_2,factor,exponent);
    strp.setPixelColor(i, mixed_color);
  }
  return;
}

/**
 * Linearly interpolates from red to green to blue aka goes through the rainbow
 * Copied from an (MIT Liscensed) AdaFruit Library
 * @param WheelPos number to represent color to be returned
 * @return Adafruit_Neopixel::Color part of the rainbow corresponding to WheelPos
 */
uint32_t Wheel(byte WheelPos) 
{
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) 
  {
    return Adafruit_NeoPixel::Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  else if(WheelPos < 170) 
  {
    WheelPos -= 85;
    return Adafruit_NeoPixel::Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  else 
  {
    WheelPos -= 170;
    return Adafruit_NeoPixel::Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
}

void Amplitude_Color(Adafruit_NeoPixel& strp, int amplitude, int t)
{
  colorStrip(strip, Adafruit_NeoPixel::Color(0,0,0,1));
  int center = strip.numPixels()/2;
  int dist; 
  dist = (int)((amplitude/1000.0)*(NUM_LED/2));
  for (int i=center-dist; i<=center+dist; i+=1)
  {
    int fact = (2*abs(i-center)+t);
    strp.setPixelColor(i,Wheel((byte)fact)/*softWhite(255*((float)dist)/NUM_LED)*/);
  }
}

void stary_night(Adafruit_NeoPixel& strp, int lightness, int probNumer, 
                 int probDenomer, int cycles)
{
  int brightness[strip.numPixels()]; //array storing bightness of each pixel
  for (int i=0; i<strip.numPixels(); i+=1) {
    brightness[i] = 0;
  }
  int randindex;
  int cycle_count;
  for (cycle_count=0; cycle_count<cycles; cycle_count+=1) {
    if (random(probDenomer) >= probDenomer-probNumer) {
      brightness[random(strip.numPixels())] = lightness;
    }
    for (int i=0; i<strip.numPixels(); i+=1) {
      strip.setPixelColor(i,strip.Color(0,0,0,brightness[i]));
      brightness[i] -= 1;
      if (brightness[i] < 0) {brightness[i] = 0;}
    }
    strip.show();
    delay(100);
  }
}

/**
 * Returns the amount of space between the heap and the stack in bytes.
 * @return amount of SRAM between the heap and stack
 */
int freeRam()
{
  extern int __heap_start, *__brkval;
  int v;
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void process_input(String input)
{
  if (input.startsWith("time"))
    while(true) 
    {
      now = rtc.now();
      timeLight(strip,now);
    }
  if (input.startsWith("set ")) {
    String args = input.substring(4);
    String sargs[]= {getValue(args, ' ', 0), getValue(args, ' ', 1),
                      getValue(args, ' ', 2), getValue(args, ' ', 3)};
    int iargs[] = {(byte)sargs[0].toInt(), (byte)sargs[1].toInt(), 
                    (byte)sargs[2].toInt(), (byte)sargs[3].toInt()};
    Serial.print("Setting...");
    Serial.println(args);
    for (int i=0; i<4; i+=1)
    {
      Serial.print(iargs[i]);
    }
    colorStrip(strip, Adafruit_NeoPixel::Color(iargs[0], iargs[1], iargs[2], iargs[3]));
    Serial.println();
  }
}

void take_input(void){
  if (!Serial.available())
  {
    String str;
    if ((str = Serial.readString()).length() > 0) 
    {
      Serial.println(str);
      process_input(str);
    } 
  }
}

void loop()
{
  int mic_read;
  mic_read = analogRead(MIC_PIN); 
  //Serial.println(mic_read);
  if (mic_read > 600 || mic_read < 400) {
    /*Serial.println("Threashold Reached");
    Serial.println(mic_read);
    Serial.println(on);*/
    on = !on;
    if (on)
      colorStrip(strip, white(100));
    else
      colorStrip(strip, white(0));
    delay(100);
  }
  


  //timeLight(strip,rtc.now());

  //take_input();
  //randomStrip(strip, 50);
  //delay(50);
  //uint32_t dim_white = white(50);
  //Serial.println(now.hour());
  //half_n_half(strip,red,blue);
  //colorStrip(strip, white(255));
  //colorStrip(strip, white(25));
  //colorStrip(strip, white(0));
  //Serial.println(freeRam());
  //colorStrip(strip, red(100),0,strip.numPixels()/2);
  //colorStrip(strip, blue(100),strip.numPixels()/2,strip.numPixels());
  //fade(strip,blue(100),0,red(100),strip.numPixels(),1);
  //colorStrip(strip,mixColors(blue(255),red(255),.01));
  strip.show();

}
