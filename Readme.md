# SK6812RGBW Neopixel Pattern Library
A small, but growing set of functions for Arduino that can be used to make an SK6812RGBW LED (or any other Adafruit_NeoPixel library compatible RGBW LEDs) strip display patterns. This library also interfaces with a real time clock (RTC_DS1307) for time of day or longer scale periodic patterns.

#### Dependencies
Arduino IDE or equivalent build tools, [Adafruit_NeoPixel library](https://github.com/adafruit/Adafruit_NeoPixel "Neopixel library"), [Adafruit RTC library](https://github.com/adafruit/RTClib "RTC library"), Doxygen (if generating documentation).

#### Build instructions
Building for Arduino without the Arduino IDE is a bit complicated and specific to the board being used, so I'll leave it to individuals to figure that out. To build and upload, open the file named `SK6812RGBW.ino` in the Arduino IDE and press the upload button.

#### Documentation
This library uses Doxygen for generating documentation. It includes a Doxygen configuration file, which can be used to generate documentation from the command line with `doxygen doxygen.config`

#### License
Licensed under [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0 "Apache License") (see LICENSE file)
